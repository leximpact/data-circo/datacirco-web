echo "Starting script at `date`"
# Stop at first error
set -e

# Si erreur `convert-im6.q16hdri: attempt to perform an operation not allowed by the security policy `PDF' @ error/constitute.c/IsCoderAuthorized/421.`
# Il faut executer `sed -i '/PDF/d' /etc/ImageMagick-6/policy.xml`

read_env() {
  echo "Reading .env config"
  local filePath="${1:-.env}"

  if [ ! -f "$filePath" ]; then
    echo "missing ${filePath}"
    exit 1
  fi

  while read -r LINE; do
    # Remove leading and trailing whitespaces, and carriage return
    CLEANED_LINE=$(echo "$LINE" | awk '{$1=$1};1' | tr -d '\r')

    if [[ $CLEANED_LINE != '#'* ]] && [[ $CLEANED_LINE == *'='* ]]; then
      export "$CLEANED_LINE"
    fi
  done < "$filePath"
}

read_env

BASE_URL=`sed -e 's/^"//' -e 's/"$//' <<<"$BASE_URL"`

rm -rf pdf/*
mkdir -p pdf
mkdir -p reduced-pdf
rm -rf reduced-pdf/*
cd pdf


echo "Start building PDF..."
for c in $(cat ../circo.txt)
do
    echo "wget -O $c.pdf \"$BASE_URL/circo-print/$c\""
    wget -O $c.pdf "$BASE_URL/circo-print/$c"
    echo "$c Cr_ation de la vignette"
    # convert $c.pdf[0] -resize 50% -background white -flatten -type palette $c.pdf.png > /dev/null
    # optipng $c.pdf.png > /dev/null 2>/dev/null
    # mv $c.pdf.png ../reduced-pdf/
    gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=../reduced-pdf/$c.pdf $c.pdf
    rm $c.pdf # remove original pdf to make space
done
echo "Done script `date` in $SECONDS s at `date`"
