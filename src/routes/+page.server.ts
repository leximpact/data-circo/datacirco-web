import type { PageServerLoad } from "./$types"

import type { ContoursCircos, PointCirco } from "$lib/circo"
import config from "$lib/server/config"

async function fetchJson(url: string) {
  const response = await fetch(url)
  if (!response.ok) {
    console.error(
      `Error while fetching JSON at URL (${url}):\n${response.status} ${response.statusText}`,
    )
    console.error(await response.text())
    return
  }
  return response.json()
}

export const load = (async () => {
  const [circoList, contoursCircos] = await Promise.all([
    fetchJson(
      new URL(
        "/datacirco/data-json/circos_list.json",
        config.dataUrl,
      ).toString(),
    ) as Promise<PointCirco[]>,
    fetchJson(
      new URL(
        "/datacirco/data-json/circonscriptions-legislatives-smaller.json",
        config.dataUrl,
      ).toString(),
    ) as Promise<ContoursCircos>,
  ])

  return {
    circoList,
    contoursCircos,
  }
}) satisfies PageServerLoad
