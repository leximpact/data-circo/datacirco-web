import config from "$lib/server/config"

import type { LayoutServerLoad } from "./$types"

export const load: LayoutServerLoad = () => {
  return {
    baseUrl: config.baseUrl,
    dataUrl: config.dataUrl,
    matomo: config.matomo,
    territoiresUrl: config.territoiresUrl,
  }
}
