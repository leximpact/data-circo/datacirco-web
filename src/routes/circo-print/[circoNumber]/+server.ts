/*
Ce fichier réalise la conversion en PDF de la page complète `/circonscriptions/${params.circoNumber}?print=true`
*/

import { chromium } from "playwright"

import config from "$lib/server/config"

import type { RequestHandler } from "./$types"

export const GET: RequestHandler = async ({ params }) => {
  /*const dirPath = `https://leximpact.an.fr/datacirco/data-json/${params.numeroCirco}`
  const response = await fetch(dirPath)
  if (response.status === 404) {
    throw error(404, "Circonscription inconnue")
  }*/

  const browser = await chromium.launch()
  const page = await browser.newPage()
  await page.goto(
    new URL(
      `/circonscriptions/${params.circoNumber}?print=true`,
      config.baseUrl,
    ).toString(),
  )

  // the locator #precipitations is visible in every circo except 075-05, which is where the locator #dpe is visible
  // const locatorFound = page.locator(".wait-for-load").nth(0)
  // await locatorFound.waitFor({ state: "visible" })

  // TODO: Fix bug. When uncommenting below and upon waiting for graphs, the fonts do not load...
  const locator = page.locator(".wait-for-load")
  // console.log("locator", locator)
  const allLocators = await locator.all()
  // console.log("locator", allLocators)
  for (const loc of allLocators) {
    // console.log("waiting for ", loc, " to become visible")
    await loc.waitFor({ state: "visible" })
    // console.log(loc, " became visible")
  }

  const pdfBuffer = await page.pdf({
    printBackground: true,
    format: "A4",
    displayHeaderFooter: true,
    headerTemplate: "<div></div>",
    // font-size: 10px est la taille de la police du numéro de page
    footerTemplate:
      '<div style="margin-right: 50px; margin-bottom: 50px; font-size: 10px; text-align: right; width: 100%;"><span class="pageNumber"></span></div>',
  })
  await browser.close()
  return new Response(pdfBuffer, {
    headers: { "Content-Type": "application/pdf" },
  })
}
