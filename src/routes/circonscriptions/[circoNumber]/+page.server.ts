import type { CircoData } from "$lib/circo"
import type { ClimatData } from "$lib/modules/environnement"
import type { EnvironnementData } from "$lib/modules/environnement"
import type { EducationData } from "$lib/modules/education"
import type { ElectionsData } from "$lib/modules/elections"
import type { EmploiData } from "$lib/modules/emploi"
import type { EntreprisesData } from "$lib/modules/entreprises"
import type { LogementData } from "$lib/modules/logement"
import type { PopulationData } from "$lib/modules/population"
import type { RevenuData } from "$lib/modules/revenu"
import type { SanteData } from "$lib/modules/sante"
import type { Source, Sources, SourcesData } from "$lib/modules/sources"
import config from "$lib/server/config"

import type { PageServerLoad } from "./$types"

async function fetchJson(url: string) {
  const response = await fetch(url)
  if (!response.ok) {
    console.error(
      `Error while fetching JSON at URL (${url}):\n${response.status} ${response.statusText}`,
    )
    console.error(await response.text())
    return
  }
  return response.json()
}

function getFile(filename: string, url: string) {
  return fetchJson(new URL(filename, url).toString())
}

async function getSources() {
  const sources = (await fetchJson(
    new URL("/datacirco/data-json/sources.json", config.dataUrl).toString(),
  )) as {
    education: Source[]
    elus: Source[]
    environnement : Source[]
    emplois: Source[]
    entreprises: Source[]
    equipements: Source[]
    geographie: Source[]
    logements: Source[]
    population: Source[]
    sante: Source[]
    revenu: Source[]
  }

  return Object.fromEntries(
    Object.entries(sources).map(([key, sourceList]) => [
      key as keyof SourcesData,
      Object.fromEntries(
        sourceList.map((source) => [
          source.id_reference ?? source.filename,
          source,
        ]),
      ) as Sources,
    ]),
  ) as unknown as SourcesData
}

const fetchAll = (url: string) =>
  Promise.all([
    getFile("common_data.json", url) as Promise<CircoData>,
    getFile("meteo.json", url) as Promise<ClimatData>,
    getFile("environnement.json", url) as Promise<EnvironnementData>,
    getFile("education.json", url) as Promise<EducationData>,
    getFile("elections.json", url) as Promise<ElectionsData>,
    getFile("emploi.json", url) as Promise<EmploiData>,
    getFile("entreprises.json", url) as Promise<EntreprisesData>,
    getFile("logement.json", url) as Promise<LogementData>,
    getFile("population.json", url) as Promise<PopulationData>,
    getFile("sante.json", url) as Promise<SanteData>,
    getFile("revenu.json", url) as Promise<RevenuData>,
    getSources() as Promise<SourcesData>,
  ])

export const load = (async ({ params }) => {
  const url = new URL(
    `${params.circoNumber}/`,
    config.dataUrl + "datacirco/data-json/",
  ).toString()

  const [
    circoData,
    climat,
    environnement,
    education,
    elections,
    emploi,
    entreprises,
    logement,
    population,
    sante,
    revenu,
    sources,
  ] = await fetchAll(url)

  return {
    circoData,
    climat,
    environnement,
    education,
    elections,
    emploi,
    entreprises,
    logement,
    population,
    sante,
    revenu,
    sources,
  }
}) satisfies PageServerLoad
