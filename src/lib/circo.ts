import type { GeoJsonObject } from "geojson"

export type CircoModule =
  | "population"
  | "elections"
  | "economie"
  | "logement"
  | "sante"
  | "environnement"
  | "education"
  | "revenus"

export const circoModules: CircoModule[] = [
  "population",
  "elections",
  "economie",
  "logement",
  "sante",
  "environnement",
  "education",
  "revenus",
]

export interface CircoSuggestion {
  autocompletion: string
  code: string
  codeDashed: string
  depute: Depute
  label?: string
  libelle: string
}

export interface Depute {
  etatCivil: { ident: { nom: string; prenom: string } }
}

// Données allégées de la circo
export interface CircoData {
  circo: string
  limites_circo: GeoJsonObject
  superficie_circo: number
  superficie_nat: number
}

// Données de carte de la circo (avec les coordonnées du centre)
export interface PointCirco {
  code_circo: string
  code_departement: string
  latitude: number
  libelle_circo: string
  longitude: number
  num_circo: number
  nom_departement: string
}

// Carte de toutes les circonscriptions législatives
export interface ContoursCircos {
  type: "FeatureCollection"
  features: {
    type: "Feature"
    geometry: {
      type: "Polygon"
      coordinates: number[][][]
    }
    properties: {
      code_circo: string
      num_circo: number
      code_departement: string
      nom_departement: string
    }
  }[]
}
