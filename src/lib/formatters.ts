// fr-CA is used instead of fr-FR to obtain a NBSP between numbers instead of a NNBSP
export const formatThousands = (n: number, fractionDigits = 2): string =>
  new Intl.NumberFormat("fr-CA", {
    maximumFractionDigits: fractionDigits,
    minimumFractionDigits: fractionDigits,
  }).format(n)

export const formatThousandsWithoutDecimal = (n: number): string =>
  new Intl.NumberFormat("fr-CA", {
    maximumFractionDigits: 0,
    minimumFractionDigits: 0,
  }).format(n)

export const formatPercent = (n: number, minimumFractionDigits:number = 1): string =>
  new Intl.NumberFormat("fr-FR", {
    minimumFractionDigits: minimumFractionDigits,
    style: "percent",
  }).format(n)

export const formatPoints = (n: number): string =>
  new Intl.NumberFormat("fr-FR", {
    minimumFractionDigits: 1,
    maximumFractionDigits: 1,
  }).format(n * 100)

export const formatDecimal = (n: number, fractionDigits = 2): string =>
  new Intl.NumberFormat("fr-FR", {
    maximumFractionDigits: fractionDigits,
    minimumFractionDigits: fractionDigits,
  }).format(n)

export function displayPercentEvolution(n1: number, n2: number) {
  if (n2 > n1) {
    return "une augmentation de ".concat(formatPercent((n2 - n1) / n1))
  }
  return "une baisse de ".concat(formatPercent(Math.abs((n2 - n1) / n1)))
}

export function difference(n1: number, n2: number) {
  let span
  if (n1 > n2) {
    span =
      '<span class="text-green-700 text-left"><span class="font-bold">&plus;</span>&nbsp;'
  } else if (n1 < n2) {
    span =
      '<span class="text-red-500 text-left"><span class="font-bold">&minus;</span>&nbsp;'
  }
  if ((Math.abs(n1 - n2) * 100).toFixed(1) === "0.0") {
    span = '<span class="text-gray-600 text-left">&nbsp;'
  }
  return (
    span +
    formatPoints(Math.abs(n1 - n2)) +
    ' pt </span> <span class="text-neutral-600 text-[0.51rem]">(' +
    formatPercent(n2) +
    ")</span>"
  )
}

export function difference_num(n1: number, n2: number) {
  let span
  if (n1 > n2) {
    span =
      '<span class="text-green-700 text-left"><span class="font-bold">&plus;</span>&nbsp;'
  } else if (n1 < n2) {
    span =
      '<span class="text-red-500 text-left"><span class="font-bold">&minus;</span>&nbsp;'
  }
  if ((Math.abs(n1 - n2) * 100).toFixed(1) === "0.0") {
    span = '<span class="text-gray-600 text-left">&nbsp;'
  }
  return (
    span +
    formatDecimal(Math.abs(n1 - n2))
    +
    " d'écart </span>"
  )
}

export function evolution(n1: number, n2: number) {
  let span
  if (n2 > n1) {
    span =
      '<span class="text-green-700"><span class="font-bold">&plus;</span>&nbsp;'
  } else if (n2 < n1) {
    span =
      '<span class="text-red-500"><span class="font-bold">&minus;</span>&nbsp;'
  }
  if (n1 === 0 || Math.abs((n2 - n1) / (n1 / 100)).toFixed(1) === "0.0") {
    return (
      '<span class="text-gray-600 text-left">&nbsp;' +
      formatPercent(0) +
      " </span>"
    )
  }
  return span + formatPoints(Math.abs((n2 - n1) / n1)) + " %</span>"
}

export function signedNumber(n: number, fractionDigits: number = 2) {
  let span
  if (n > 0) {
    span =
      '<span class="text-green-700 text-left"><span class="font-bold">&plus;</span>&nbsp;'
  } else if (n < 0) {
    span =
      '<span class="text-red-500 text-left"><span class="font-bold">&minus;</span>&nbsp;'
  } else {
    span = '<span class="text-gray-600 text-left">&nbsp;'
  }
  return span + formatThousands(Math.abs(n), fractionDigits) + "</span>"
}

export function formatRatio(
  n1: number,
  n2: number,
  fractionDigits: number = 2,
  lineBreak: boolean=false,
) {
  let span
  if (n1 > n2) {
    span = `<span class="text-red-500 text-left">${formatDecimal(
      n1 / n2,
      1,
    )}&nbsp;fois&nbsp;moins</span>`
  } else if (n1 === n2) {
    span = '<span class="text-left font-bold">Autant</span>'
  } else {
    span = `<span class="text-green-700 text-left">${formatDecimal(
      n2 / n1,
      1,
    )}&nbsp;fois&nbsp;plus</span>`
  }
  if (lineBreak){
    return (
      span + '<br/>' +
      `<span class="text-neutral-600 text-[0.51rem]"> (1 pour ` +
      formatThousands(parseInt(String(n2)), fractionDigits) +
      ") </span>"
    )
  }else{
    return (
      span +
      `<span class="text-neutral-600 text-[0.51rem]"> (1 pour ` +
      formatThousands(parseInt(String(n2)), fractionDigits) +
      ") </span>"
    )
  }
}

export function displayPoints(n: number): string | undefined {
  if ((Math.abs(n) * 100).toFixed(1) === "0.0") {
    return "équivalente"
  }
  let span
  if (n > 0) {
    span = "supérieure de "
  } else if (n < 0) {
    span = "inférieure de "
  }
  span += formatPoints(Math.abs(n)) + " point"
  if (Math.abs(n * 100) > 1) {
    span += "s"
  }
  return span
}

export function depCirco(numcirco: string) {
  let dep
  if (numcirco.startsWith("0")) {
    dep = numcirco.substring(1).slice(-2)
  } else {
    dep = numcirco.substring(1)
  }
  return dep
}
