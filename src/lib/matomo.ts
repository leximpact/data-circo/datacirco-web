import { browser } from "$app/environment"

export function init({
  prependDomain,
  siteId,
  subdomains,
  url,
}: {
  prependDomain?: boolean | undefined
  siteId: number
  subdomains?: string | undefined
  url: string
}) {
  // @ts-expect-error: global variable needed by Matomo.
  window._paq ??= []
  // @ts-expect-error: global variable needed by Matomo.
  const _paq = window._paq
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  if (subdomains !== undefined) {
    _paq.push(["setCookieDomain", subdomains])
    _paq.push(["setDomains", subdomains])
  }
  _paq.push([
    "setDocumentTitle",
    prependDomain ? document.domain + "/" + document.title : document.title,
  ])
  _paq.push(["trackPageView"])
  _paq.push(["enableLinkTracking"])
  ;(function () {
    _paq.push(["setTrackerUrl", url + "matomo.php"])
    _paq.push(["setSiteId", siteId])
    const d = document,
      g = d.createElement("script"),
      s = d.getElementsByTagName("script")[0]
    g.type = "text/javascript"
    g.async = true
    g.defer = true
    g.src = url + "matomo.js"
    s?.parentNode?.insertBefore(g, s)
  })()
}

function trackEvent(category: string, action: string, name: string) {
  if (browser) {
    // @ts-expect-error: global variable needed by Matomo.
    window._paq ??= []
    // @ts-expect-error: global variable needed by Matomo.
    const _paq = window._paq
    _paq.push(["trackEvent", category, action, name])
  }
}

/**
 * Export interactions - PDF / ZIP
 */

export function trackPDF(circoNumber: string) {
  trackEvent(
    "export",
    "pdf_download",
    `Clic téléchargement du PDF (${circoNumber})`,
  )
}

export function trackZIP(circoNumber: string) {
  trackEvent(
    "export",
    "zip_download",
    `Clic téléchargement du ZIP des données (${circoNumber})`,
  )
}
