import type { Source } from "$lib/modules/sources"

export interface Entreprise {
  categorieentreprise: string
  nombre: number
  employeur: number
  sum_siren: number
}

export interface EntreprisesSiege {
  categorieentreprise: string
  nombre: number
}

export interface Employeur {
  siren: string
  nom: string
  trancheeffectifsetablissement: string
  categorieentreprise: string
  commune: string
  latitude: number
  longitude: number
}

export interface EtablissementsCategorie {
  categorieentreprise: string
  nombre: number
}

export interface Etablissement {
  trancheeffectifsetablissement: string
  tranche_lib: string
  nombre: number
}

export interface Immatriculation {
  annee: number
  immatriculations: number
}
export interface Radiation {
  annee: number
  radiations: number
}

export interface ImmatRad {
  annee_mois: string
  immatriculations: number
  radiations: number
  solde: number
}

export interface EntreprisesData {
  tranche_effectifs: { [tranche: string]: string }
  sources: Source[]
  entreprises: Entreprise[]
  entreprises_sieges: EntreprisesSiege[]
  etablissements_par_categorie: EtablissementsCategorie[]
  etablissements: Etablissement[]
  employeurs: Employeur[]
  rncs_immatriculations?: Immatriculation[]
  rncs_immatriculations_nat: Immatriculation[]
  rncs_radiations?: Radiation[]
  rncs_radiations_nat: Radiation[]
  immat_rad: ImmatRad[]
}
