import type { Source } from "$lib/modules/sources"

export interface Salarie {
  secteur_full: string
  sous_secteur_full: string
  secteur: string
  sous_secteur: string
  effectifs_2006: number
  effectifs_2021: number
  effectifs_2022: number
  effectifs_2023: number
}

export interface Secteur {
  libelle_complet: string
  libelle: string
  effectifs_2021: number
  effectifs_2022: number
  effectifs_2023: number
}

export interface Emploi {
  geojson: string
  nom: string
  evolution: number
  longitude: number
  latitude: number
}

export interface EmploiData {
  emplois: Emploi[]
  salaries: Salarie[]
  secteurs: Secteur[]
  sources: Source[]
  total_salaries: number
}
