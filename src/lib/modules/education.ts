import type { Data, Layout } from "plotly.js"

import type { Plotly } from "$lib/plotly"

interface ColorMap {
  [key: string]: string
}

// Correspondance entre les couleurs et les valeurs HTML
const color_map: ColorMap = {
  cyan: "#00E0D7",
  blue: "#1132FC",
  violet: "#7800F1",
  fuchsia: "#D40EC3",
  red: "#C50140",
  orange: "#FF553E",
  goldenrod: "#FFAB01",
  grey: "#686868",
}

export interface EducationData {
  // Effectifs France
  effectifs_premier_et_second_france: number
  effectifs_premier_et_second_prive_france: number
  effectifs_primaire_france: number
  effectifs_primaire_prive_france: number
  effectifs_college_france: number
  effectifs_college_prive_france: number
  effectifs_lycee_france: number
  effectifs_lycee_prive_france: number
  effectifs_superieur_france: number
  effectifs_superieur_prive_france: number
  // Effectifs public + privé circo
  total_effectifs_primaire: number
  total_effectifs_college: number
  total_effectifs_lycee: number
  total_effectifs_superieur: number
  total_effectifs_autre: number
  total_effectifs_primaire_et_secondaire: number
  // Effectifs privé
  total_effectifs_primaire_prive: number
  total_effectifs_college_prive: number
  total_effectifs_lycee_prive: number
  total_effectifs_superieur_prive: number
  total_effectifs_autre_prive: number
  total_effectifs_primaire_et_secondaire_prive: number
  // IPS
  ips_premier_et_second_circo_list: number[]
  ips_premier_et_second_public_circo_list: number[]
  ips_premier_et_second_prive_circo_list: number[]
  ips_premier_et_second_public_france: number
  ips_premier_et_second_prive_france: number
  ips_premier_et_second_france: number
  ips_premier_et_second_public_circo: number
  ips_premier_et_second_prive_circo: number
  ips_premier_et_second_circo_avg: number
  ips_premier_et_second_france_max: number
  ips_premier_et_second_france_min: number
  ips_premier_et_second_prive_france_max: number
  ips_premier_et_second_prive_france_min: number
  ips_premier_et_second_public_france_max: number
  ips_premier_et_second_public_france_min: number
  ips_premier_et_second_circo_max: number
  ips_premier_et_second_circo_min: number
  ips_premier_et_second_prive_circo_max: number
  ips_premier_et_second_prive_circo_min: number
  ips_premier_et_second_public_circo_max: number
  ips_premier_et_second_public_circo_min: number

  nombre_etablissement: number
  total_effectifs: number
  ips_mean: number
  etablissements_premier_et_second_degre: EducationInfoPS[]
  liste_primaires: EducationInfoPS[]
  liste_colleges: EducationInfoPS[]
  liste_lycees: EducationInfoPS[]
  liste_secondaires: EducationInfoPS[]
  liste_autres: EducationInfoPS[]
  etablissements_superieur: EducationInfoSup[]
  etablissements_tous: EducationInfoPS[]
}

export interface EducationInfoPS {
  latitude: number
  longitude: number
  appellation_officielle: string
  secteur_public_prive_libe: string
  nature_uai_libe: string // Unique to primary and secondary schools
  ips: number // Unique to primary and secondary schools
  effectifs: number
  taux_de_reussite: number // Unique to primary and secondary schools
  valeur_ajoutee: number // Unique to primary and secondary schools
  libelle_commune: string
  etab_prioritaire?: string
  // Pour les établissements supérieurs
  type_etablissement?: string
  tutelle?: string
  site_internet?: string
  url_et_id_onisep?: string
  appariement?: string // Unique to primary and secondary schools
}

export interface EducationInfoSup {
  latitude: number
  longitude: number
  appellation_officielle: string // Pour les établissements supérieurs
  type_etablissement: string // Pour les établissements supérieurs
  secteur_public_prive_libe: string
  effectifs: number
  libelle_commune: string
  tutelle: string
  site_internet: string
  url_et_id_onisep: string
}

export function buildEducationData(dataEducation: EducationData) {
  // On converti les données pour les établissements supérieurs au format des établissements primaires et secondaires, pour avoir une seule liste
  let etablissements_superieur_tmp: EducationInfoSup[] = []
  if (dataEducation.etablissements_superieur) {
    etablissements_superieur_tmp = dataEducation.etablissements_superieur.map(
      ({
        latitude: latitude,
        longitude: longitude,
        effectifs: effectifs,
        secteur_public_prive_libe: secteur_public_prive_libe,
        type_etablissement: nature_uai_libe,
        appellation_officielle: appellation_officielle,
        libelle_commune: libelle_commune,
        tutelle: tutelle,
        site_internet: site_internet,
        url_et_id_onisep: url_et_id_onisep,
      }) => ({
        latitude,
        longitude,
        effectifs,
        secteur_public_prive_libe,
        nature_uai_libe,
        appellation_officielle,
        libelle_commune,
        tutelle,
        site_internet,
        url_et_id_onisep,
      }),
    )
  }
  dataEducation.etablissements_tous =
    dataEducation.etablissements_premier_et_second_degre.concat(
      etablissements_superieur_tmp || [],
    )
  // École primaire  = maternelle et élémentaire
  dataEducation.liste_primaires =
    dataEducation.etablissements_premier_et_second_degre.filter(
      (d) =>
        d.nature_uai_libe === "ECOLE DE NIVEAU ELEMENTAIRE" ||
        d.nature_uai_libe === "ECOLE MATERNELLE",
    )
  dataEducation.liste_colleges =
    dataEducation.etablissements_premier_et_second_degre.filter((d) =>
      d.nature_uai_libe.startsWith("COLLEGE"),
    )
  dataEducation.liste_lycees =
    dataEducation.etablissements_premier_et_second_degre.filter((d) =>
      d.nature_uai_libe.startsWith("LYCEE"),
    )
  dataEducation.liste_secondaires = dataEducation.liste_colleges.concat(
    dataEducation.liste_lycees,
  )
  // Other schools
  dataEducation.liste_autres =
    dataEducation.etablissements_premier_et_second_degre.filter(
      (d) =>
        !(
          d.nature_uai_libe === "ECOLE DE NIVEAU ELEMENTAIRE" ||
          d.nature_uai_libe === "ECOLE MATERNELLE" ||
          d.nature_uai_libe.startsWith("COLLEGE") ||
          d.nature_uai_libe.startsWith("LYCEE")
        ),
    )
  dataEducation.total_effectifs_autre = dataEducation.liste_autres.reduce(
    (a, b) => Number(a) + Number(b.effectifs),
    0,
  )

  dataEducation.total_effectifs_primaire = dataEducation.liste_primaires.reduce(
    (acc, cur) => Number(acc) + Number(cur.effectifs || 0),
    0,
  )
  dataEducation.total_effectifs_college = dataEducation.liste_colleges.reduce(
    (acc, cur) => Number(acc) + Number(cur.effectifs || 0),
    0,
  )
  dataEducation.total_effectifs_lycee = dataEducation.liste_lycees.reduce(
    (acc, cur) => Number(acc) + Number(cur.effectifs || 0),
    0,
  )
  // --------- IPS ---------
  const liste_etab_prim_second_public_ips =
    dataEducation.etablissements_premier_et_second_degre.filter(
      (d) => d.secteur_public_prive_libe === "public" && Number(d.ips || 0) > 1,
    )
  const liste_etab_prim_second_prive_ips =
    dataEducation.etablissements_premier_et_second_degre.filter(
      (d) => d.secteur_public_prive_libe != "public" && Number(d.ips || 0) > 1,
    )
  dataEducation.ips_premier_et_second_prive_circo_list =
    liste_etab_prim_second_prive_ips.map((d) => d.ips)
  dataEducation.ips_premier_et_second_circo_list =
    dataEducation.etablissements_premier_et_second_degre
      .filter((d) => Number(d.ips || 0) > 1)
      .map((d) => Number(d.ips))
  dataEducation.ips_premier_et_second_circo_max = Math.max(
    ...dataEducation.ips_premier_et_second_circo_list,
  )
  dataEducation.ips_premier_et_second_circo_min = Math.min(
    ...dataEducation.ips_premier_et_second_circo_list,
  )

  dataEducation.ips_premier_et_second_prive_circo_max = Math.max(
    ...liste_etab_prim_second_prive_ips.map((d) => Number(d.ips)),
  )
  dataEducation.ips_premier_et_second_prive_circo_min = Math.min(
    ...liste_etab_prim_second_prive_ips.map((d) => Number(d.ips)),
  )
  dataEducation.ips_premier_et_second_public_circo_max = Math.max(
    ...liste_etab_prim_second_public_ips.map((d) => Number(d.ips)),
  )
  dataEducation.ips_premier_et_second_public_circo_min = Math.min(
    ...liste_etab_prim_second_public_ips.map((d) => Number(d.ips)),
  )

  dataEducation.ips_premier_et_second_public_circo_list =
    liste_etab_prim_second_public_ips.map((d) => d.ips)
  dataEducation.ips_premier_et_second_public_circo =
    liste_etab_prim_second_public_ips.reduce(
      (acc, cur) => acc + Number(cur.ips),
      0,
    ) / liste_etab_prim_second_public_ips.length

  dataEducation.ips_premier_et_second_prive_circo =
    liste_etab_prim_second_prive_ips.reduce(
      (acc, cur) => acc + Number(cur.ips),
      0,
    ) / (liste_etab_prim_second_prive_ips.length || 1)
  // Compute the mean of IPS score of all schools
  let nb_etab_with_ips = 0
  const ips_sum = dataEducation.etablissements_premier_et_second_degre.reduce(
    (a, b) => {
      if (Number(b.ips)) {
        nb_etab_with_ips++
        return Number(a) + Number(b.ips)
      } else {
        return Number(a)
      }
    },
    0,
  )
  dataEducation.ips_premier_et_second_circo_avg = ips_sum / nb_etab_with_ips

  if (dataEducation.etablissements_superieur) {
    dataEducation.total_effectifs_superieur =
      dataEducation.etablissements_superieur.reduce(
        (acc, cur) => Number(acc) + Number(cur.effectifs || 0),
        0,
      )
  } else {
    dataEducation.total_effectifs_superieur = 0
    // TODO: Trouver l'indicateur Public/privé pour les effectifs des établissements supérieurs
    dataEducation.total_effectifs_superieur_prive = 0
  }
  dataEducation.nombre_etablissement =
    dataEducation.etablissements_premier_et_second_degre.length +
    (dataEducation.etablissements_superieur?.length || 0)
  dataEducation.total_effectifs =
    dataEducation.total_effectifs_primaire +
    dataEducation.total_effectifs_college +
    dataEducation.total_effectifs_lycee +
    dataEducation.total_effectifs_superieur +
    dataEducation.total_effectifs_autre
  dataEducation.total_effectifs_primaire_et_secondaire =
    dataEducation.total_effectifs_primaire +
    dataEducation.total_effectifs_college +
    dataEducation.total_effectifs_lycee
  // Compute the effectif of private schools
  dataEducation.total_effectifs_primaire_prive =
    dataEducation.liste_primaires.reduce((a, b) => {
      if (b.secteur_public_prive_libe != "public") {
        return Number(a) + Number(b.effectifs || 0)
      } else {
        return Number(a)
      }
    }, 0)
  dataEducation.total_effectifs_college_prive =
    dataEducation.liste_colleges.reduce((a, b) => {
      if (b.secteur_public_prive_libe != "public") {
        return Number(a) + Number(b.effectifs || 0)
      } else {
        return Number(a)
      }
    }, 0)
  dataEducation.total_effectifs_lycee_prive = dataEducation.liste_lycees.reduce(
    (a, b) => {
      if (b.secteur_public_prive_libe != "public") {
        return Number(a) + Number(b.effectifs || 0)
      } else {
        return Number(a)
      }
    },
    0,
  )

  dataEducation.total_effectifs_autre_prive = dataEducation.liste_autres.reduce(
    (a, b) => {
      if (b.secteur_public_prive_libe != "public") {
        return Number(a) + Number(b.effectifs || 0)
      } else {
        return Number(a)
      }
    },
    0,
  )
  dataEducation.total_effectifs_primaire_et_secondaire_prive =
    dataEducation.total_effectifs_primaire_prive +
    dataEducation.total_effectifs_college_prive +
    dataEducation.total_effectifs_lycee_prive
}

export function getColorFromEffectif(effectifs: number | null): string {
  if (effectifs === null) {
    return color_map["grey"]
  }
  effectifs = Number(effectifs || 0)
  let color: string
  if (effectifs < 1) {
    color = "grey"
  } else if (effectifs < 100) {
    color = "goldenrod"
  } else if (effectifs < 200) {
    color = "orange"
  } else if (effectifs < 300) {
    color = "red"
  } else if (effectifs < 500) {
    color = "fuchsia"
  } else if (effectifs < 800) {
    color = "violet"
  } else if (effectifs < 1000) {
    color = "blue"
  } else {
    color = "cyan"
  }
  return color_map[color]
}

export function getColorFromIps(ips: number | undefined): string {
  if (ips === undefined) {
    return color_map["grey"]
  }
  ips = Number(ips || 0)

  let color: string
  if (ips < 25) {
    color = "grey"
  } else if (ips < 90) {
    color = "goldenrod"
  } else if (ips < 100) {
    color = "orange"
  } else if (ips < 110) {
    color = "red"
  } else if (ips < 120) {
    color = "fuchsia"
  } else if (ips < 130) {
    color = "violet"
  } else if (ips < 140) {
    color = "blue"
  } else {
    color = "cyan"
  }
  return color_map[color]
}

export function plotEffectifs(dataEducation: EducationData, plotly: Plotly) {
  /*Carte de l'effectif des écoles primaires, secondaires et supérieures */
  const hovertemplate =
    "%{customdata.appellation_officielle}<br>Statut : %{customdata.secteur_public_prive_libe}<br>%{customdata.nature_uai_libe}<br>Effectif : %{customdata.effectifs}<extra></extra>"
  const color = dataEducation.etablissements_tous.map((d) =>
    getColorFromEffectif(d.effectifs),
  )

  plotMap(
    "plot_effectifs",
    dataEducation.etablissements_tous,
    hovertemplate,
    color,
    plotly,
  )
}

export function plotIps(dataEducation: EducationData, plotly: Plotly) {
  /*Carte de l'IPS des écoles primaires et secondaires*/

  const hovertemplate =
    "%{customdata.appellation_officielle}<br>Statut : %{customdata.secteur_public_prive_libe}<br>%{customdata.nature_uai_libe}<br>Indices de position sociale : %{customdata.ips}<extra></extra>"
  const color = dataEducation.etablissements_premier_et_second_degre.map((d) =>
    getColorFromIps(d.ips),
  )

  plotMap(
    "plot_ips",
    dataEducation.etablissements_premier_et_second_degre,
    hovertemplate,
    color,
    plotly,
  )
}

export function plotIpsHisto(
  div_name: string,
  customdata: EducationData,
  plotly: Plotly,
) {
  const data: Data[] = [
    {
      type: "histogram",
      y: customdata.ips_premier_et_second_public_circo_list,
      name: "public",
      opacity: 0.5,
      marker: {
        color: "#635f06",
      },
    },
    {
      type: "histogram",
      histfunc: "sum",
      orientation: "h",
      marker: {
        color: "#2F406A",
      },
      opacity: 0.5,
      x: Array(customdata.ips_premier_et_second_prive_circo_list.length).fill(
        -1,
      ),
      yaxis_autorange: "reversed",
      y: customdata.ips_premier_et_second_prive_circo_list,

      name: "Privé",
    },
  ]
  const ips_premier_et_second_prive_france = [
    customdata.ips_premier_et_second_prive_france_max,
    customdata.ips_premier_et_second_prive_france_min,
    customdata.ips_premier_et_second_prive_france,
  ]
  const ips_premier_et_second_france_labels = [
    "France IPS max",
    "France IPS min",
    "France IPS moyen",
  ]
  const shapes_ips_prive_france = ips_premier_et_second_prive_france.map(
    (ips) => ({
      type: "line",
      x0: 0,
      y0: ips,
      x1: -15,
      y1: ips,
      line: {
        color: "gray",
        width: 1,
        dash: "dot",
      },
    }),
  )
  const annotations_prive_france = ips_premier_et_second_prive_france.map(
    (ips) => ({
      x: -18,
      y: ips,
      xref: "x",
      yref: "y",
      ax: 0,
      ay: 0,
      text: ips_premier_et_second_france_labels[
        ips_premier_et_second_prive_france.indexOf(ips)
      ],
      font: {
        size: 10,
        color: "gray",
      },
      align: "left",
    }),
  )

  const ips_premier_et_second_public_france = [
    customdata.ips_premier_et_second_public_france_max,
    customdata.ips_premier_et_second_public_france_min,
    customdata.ips_premier_et_second_public_france,
  ]

  const shapes_ips_public_france = ips_premier_et_second_public_france.map(
    (ips) => ({
      type: "line",
      x0: 15,
      y0: ips,
      x1: 0,
      y1: ips,
      line: {
        color: "gray",
        width: 1,
        dash: "dot",
      },
    }),
  )
  const annotations_public_france = ips_premier_et_second_public_france.map(
    (ips) => ({
      x: 18,
      y: ips,
      xref: "x",
      yref: "y",
      ax: 0,
      ay: 0,
      text: ips_premier_et_second_france_labels[
        ips_premier_et_second_public_france.indexOf(ips)
      ],
      font: {
        size: 10,
        color: "gray",
      },
    }),
  )
  const shape_ips_public_circo = {
    type: "line",
    x0: 10,
    y0: customdata.ips_premier_et_second_public_circo,
    x1: 0,
    y1: customdata.ips_premier_et_second_public_circo,
    line: {
      color: "#424004",
      width: 2,
      dash: "dot",
    },
  }
  const shape_ips_prive_circo = {
    type: "line",
    x0: 0,
    y0: customdata.ips_premier_et_second_prive_circo,
    x1: -10,
    y1: customdata.ips_premier_et_second_prive_circo,
    line: {
      color: "#2F406A",
      width: 2,
      dash: "dot",
    },
  }
  const annotations_prive_circo = {
    x: -13,
    y: customdata.ips_premier_et_second_prive_circo,
    xref: "x",
    yref: "y",
    ax: 0,
    ay: 0,
    text: "IPS moyen",
    font: {
      size: 10,
      color: "#2F406A",
    },
    align: "left",
  }

  const annotations_public_circo = {
    x: 13,
    y: customdata.ips_premier_et_second_public_circo,
    xref: "x",
    yref: "y",
    ax: 0,
    ay: 0,
    text: "IPS moyen",
    font: {
      size: 10,
      color: "#424004",
      textalign: "left",
    },
    position: "left",
  }
  const layout: Layout = {
    //title: "Répartition des établissements par IPS",
    width: 800,
    height: 500,
    margin: {
      t: 30,
    },
    paper_bgcolor: "rgba(0,0,0,0)",
    barmode: "overlay",
    bargap: 0.0,
    bargroupgap: 0.0,
    xaxis: {
      title: "Nombre d'établissements",
      tickvals: [-20, -15, -10, -5, 0, 5, 10, 15, 20],
      ticktext: ["20", "15", "10", "5", "0", "5", "10", "15", "20"],
    },
    yaxis: {
      title: "IPS",
      zeroline: false,
    },
    shapes: [
      ...shapes_ips_public_france,
      shape_ips_public_circo,
      shape_ips_prive_circo,
      ...shapes_ips_prive_france,
    ],
    annotations: [
      ...annotations_prive_france,
      annotations_public_circo,
      annotations_prive_circo,
      ...annotations_public_france,
    ],
  }

  plotly.newPlot(div_name, data, layout)
}
