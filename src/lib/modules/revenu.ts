import type { Source } from "$lib/modules/sources"
import type { Polygon, MultiPolygon } from "geojson"

export interface Geometrie {
    type: string,
    coordinates: number[],
}

export interface Carreau {
    latitude_centre: number,
    longitude_centre: number,
    geometrie: Polygon | MultiPolygon,
    pourcentage_menage_pauvre: number,
    niveau_vie_moyen_hab: number,
}

export interface Echelle {
    circonscription: number,
    departement: number,
    france: number
}

export interface General {
    taux_pauvrete: Echelle,
    decile_1: Echelle,
    mediane: Echelle,
    decile_9: Echelle
}

export interface DecompositionRevenu {
    p_revenus_activites: Echelle,
    p_pensions_retraites_rentes: Echelle,
    p_patrimoine: Echelle,
    p_presta_soc: Echelle,
    p_impots: Echelle,
}

export interface TxPauvreteTranches {
    moins_30ans: Echelle,
    age30_39: Echelle,
    age40_49: Echelle,
    age_50_59: Echelle,
    age_60_74: Echelle,
    plus_75: Echelle,
}

export interface RevenuData {
    sources: Source[],
    liste_carreaux: Carreau[],
    dict_general: General,
    dict_deciles: {},
    dict_decomposition_revenu: DecompositionRevenu,
    dict_tx_pauvrete_age: TxPauvreteTranches,
}