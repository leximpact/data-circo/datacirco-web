import type { Source } from "$lib/modules/sources"

interface Stat {
  sum_ind_snv: number
  sum_men: number
  sum_men_1ind: number
  sum_men_5ind: number
  sum_men_coll: number
  sum_men_fmp: number
  sum_men_mais: number
  sum_men_pauv: number
  sum_men_prop: number
}

export interface PopulationData {
  population_france_2018: number
  sources: Source[]
  population_circo_detail_2015?: {
    annee: number
    ind: number
    ind_0_3: number
    ind_4_5: number
    ind_6_10: number
    ind_11_17: number
    ind_18_24: number
    ind_25_39: number
    ind_40_54: number
    ind_55_64: number
    ind_65_79: number
    ind_80p: number
  }
  population_circo_detail_2018?: {
    annee: number
    ind: number
    ind_0_3: number
    ind_4_5: number
    ind_6_10: number
    ind_11_17: number
    ind_18_24: number
    ind_25_39: number
    ind_40_54: number
    ind_55_64: number
    ind_65_79: number
    ind_80p: number
  }
  population_circo_totale_2013: number
  population_circo_totale_2019: number
  population_circo_totale: number
  population_departement_detail_2018: {
    ind: number
    ind_0_3: number
    ind_4_5: number
    ind_6_10: number
    ind_11_17: number
    ind_18_24: number
    ind_25_39: number
    ind_40_54: number
    ind_55_64: number
    ind_65_79: number
    ind_80p: number
  }
  population_france_detail_2018: {
    ind: number
    ind_0_3: number
    ind_4_5: number
    ind_6_10: number
    ind_11_17: number
    ind_18_24: number
    ind_25_39: number
    ind_40_54: number
    ind_55_64: number
    ind_65_79: number
    ind_80p: number
  }
  population_circo_detail_2010: {
    annee: number
    ind: number
    ind_0_3: number
    ind_4_5: number
    ind_6_10: number
    ind_11_17: number
    ind_18_24: number
    ind_25_64: number
    ind_65p: number
  }
  carreaux_evolution_population_2010_vs_2015: [
    {
      pop_2010: number
      pop_2015: number
      geojson: { type: string; coordinates: [number, number][][] }
    },
  ]
  densite_tres_peu_dense_2015: number
  densite_peu_dense_2015: number
  densite_intermediaire_2015: number
  densite_densement_peuple_2015: number
  carreaux_densite_population_2015: [
    {
      densite: number
      geojson: { type: string; coordinates: [number, number][][] }
    },
  ]
  densite_tres_peu_dense_2018: number
  densite_peu_dense_2018: number
  densite_intermediaire_2018: number
  densite_densement_peuple_2018: number
  carreaux_densite_population_2018: [
    {
      densite: number
      geojson: { type: string; coordinates: [number, number][][] }
    },
  ]
  densite_tres_peu_dense_2020: number
  densite_peu_dense_2020: number
  densite_intermediaire_2020: number
  densite_densement_peuple_2020: number
  carreaux_densite_population_2020: [
    {
      densite: number
      geojson: { type: string; coordinates: [number, number][][] }
    },
  ]
  stats_menages_circo_2015: Stat
  stats_menages_departement_2015: Stat
  stats_menages_france_2015: Stat
  stats_menages_circo_2010: Stat
  stats_menages_departement_2010: Stat
  stats_menages_france_2010: Stat
}
