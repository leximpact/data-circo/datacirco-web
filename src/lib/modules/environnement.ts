import type { Source } from "$lib/modules/sources"
import type { Feature } from "geojson"

export interface ClimatData {
  df_precipitation: { [name: string]: { [index: number]: number } }
  df_temperature: { [name: string]: { [index: number]: number } }
  historique: { [name: string]: { [index: number]: number } }
  historique_temp_mens: {
    temp_mois_annuelle: {
      [key: string]: number[]
    }
    avg_temperature_circo: number[]
    avg_temperature_circo_1991_2020: number[]
    avg_temperature_fr: number[]
    delta_circo_france_temperature: number[]
  }
  // Le dictionnaire externe peut contenir :
  // des dictionnaires de listes ou des listes.
  historique_precip_mens: {
    precip_mois_annuelle: {
      [key: string]: number[]
    }
    avg_precipitation_circo: number[]
    avg_precipitation_fr: number[]
    delta_circo_france_precipitation: number[]
  }
  precip_2024_circo: number
  precip_2024_fr: number
  sources: Source[]
  temp_2024_circo: number
  temp_2024_fr: number
  temp_2024_monde: number
}

export interface SitesPollues {
  nom: string
  statut: string
  description: string
  nom_commune: string
  action: string
  milieu: string
  geojson: Feature
}

export interface SitesPolluesFlat {
  nom: string
  statut: string
  description: string
  nom_commune: string
  action: string
  milieu: string
  latitude: number | undefined
  longitude: number | undefined
}

export interface LocalisationPaysage {
  nom_occupation: string
  couleur: string
  geojson: Feature
}

export interface TableauOccupation {
  code_occupation: number
  nom_occupation: string
  surface_circo: number
  aire_oc_circo_ha: number
  part_superficie_circo: number
  surface_totale_france_ha: number
  aire_oc_france_ha: number
  part_superficie_france: number
}

export interface DetailArtificialisation {
  [key: string]: number
  nb_mixte: number
  nb_routiere: number
  nb_ferroviaire: number
  nb_inconnue: number
  nb_activite: number
  nb_habitat: number
}

export interface DetailArtificialisationPop {
  [key: string]: number
  art1420: number
  mepart1420: number
  menhab1420: number
  artpop1420: number
}

export interface Echelle {
  circonscription: DetailArtificialisation | DetailArtificialisationPop
  departement: DetailArtificialisation | DetailArtificialisationPop
  national: DetailArtificialisation | DetailArtificialisationPop
}
export interface GrapheArtificialisation {
  mix: number[]
  rou: number[]
  fer: number[]
  inc: number[]
  act: number[]
  hab: number[]
  total: number[]
}

export interface EnvironnementData {
  sources: Source[]
  dictionnaire_sites_pollues: SitesPollues[]
  nb_sites_pollues_circo: string
  nb_sites_pollues_dep: string
  nb_sites_pollues_nat: string
  loc_occup_sols: LocalisationPaysage[]
  nb_occup_sols: TableauOccupation[]
  tableau_artificialisation: Echelle
  tableau_artificialisation_pop: Echelle
  serie_tmp_artif: GrapheArtificialisation
}
