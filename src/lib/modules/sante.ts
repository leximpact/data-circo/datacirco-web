import type { Source } from "$lib/modules/sources"
import type { Feature } from "geojson"

export interface UrgenceMap {
  type_equip: string
  temps: number
  geojson: string
}

export interface EquipSante {
  categ_code: string
  categ_lib: string
  nb: number
  count: number
}

export interface EtabSante {
  categ_niv3_code: string
  categ_niv3_lib: string
  count: number
}

export interface ProfSante {
  profession: string
  nb_circo: number
  nb_france: number
}

export interface Urgence {
  type: string
  tps: number
  nb: number | string
  pop: number
}

export interface EML {
  equipement: string
  nb: number
}

export interface UrgenceEquip {
  code_postal: string
  nom: string
  latitude: number
  longitude: number
  type_equip: string
}

export interface UrgenceEquip {
  code_postal: string
  nom: string
  latitude: number
  longitude: number
  type_equip: string
}

export interface NiveauProxiUrgence {
  code_postal: string
  nom: string
  type_equip: string
  temps: number
  temps_heli: number
  geojson: Feature
}

export interface SanteData {
  carte_urgences: UrgenceMap[]
  equip_sante: EquipSante[]
  etab_sante: EtabSante[]
  finess_eml: EML[]
  population: number
  population_totale: number
  prof_sante: ProfSante[]
  sources: Source[]
  urgences: Urgence[]
  geoloc_equip_urgence: UrgenceEquip[]
  carte_urgences_detail: NiveauProxiUrgence[]
}
