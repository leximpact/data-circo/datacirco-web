import type { Source } from "$lib/modules/sources"

export interface Elu {
  organisation: string
  ordre: number
  femmes_dep: number
  total_dep: number
  femmes_nat: number
  total_nat: number
}

export interface ElectionsData {
  elus: Elu[]
  sources: Source[]
}
