import type { Source } from "$lib/modules/sources"

export interface GeoJson {
  type: string
  coordinates: [number, number][][] | [number, number][]
}

export interface Carreau {
  geojson: GeoJson
  pct_logement_collectifs: number
  pct_proprietaires: number
}

export interface Dpe {
  classe_consommation_energie: string
  nb: number
}

export interface Ges {
  classe_ges: string
  nb: number
}

export interface ListeAnnee {
  classe: string
  2013?: number
  2014?: number
  2015?: number
  2016?: number
  2017?: number
  2018?: number
  2019?: number
  2020?: number
  annee_2021_avant?: number
  annee_2021_apres?: number
  2021?: number
  2022?: number
  2023?: number
}


export interface Part {
  log_res: number
  log_sec: number
  log_vac: number
  proprio: number
  locatai: number
  gratuit: number
  maison: number
  ach90: number
  mfuel : number
}

export interface Nombre {
  nb_logements : number
  nb_residences_principales : number
  nb_residences_secondaires : number
  nb_maisons : number
  nb_appart : number
  nb_propretaires : number
  nb_locataires : number
  nb_gratuit : number
  nb_vacants : number
  nb_construits_avant90 : number
}

export interface Echelle {
  circonscription: Part | Nombre;
  departement: Part | Nombre;
  national: Part | Nombre;
  nb_logements: Nombre;
}

export interface LogementData {
  carreaux: Carreau[]
  data_logements_2018: Echelle
  data_logements_2020: Echelle
  dpe: Dpe[]
  dpe_communes: ListeAnnee[] | null
  dpe_post_2021: ListeAnnee[] | null
  dpe_annee: ListeAnnee[]
  dpe_dep: Dpe[]
  dpe_nat: Dpe[]
  sources: Source[]
  total_dpe: number
  total_dpe_dep: number
  total_dpe_nat: number
  ges_circo: Ges[]
  ges_dep: Ges[]
  ges_nat: Ges[]
  ges_annee: ListeAnnee[]
  total_ges: number
  total_ges_dep: number
  total_ges_nat: number
}
