export interface Source {
  date_download: string // 9
  description: string // 2
  filename: string // 0
  last_modified: string // 8
  licence: string // 5
  module: string // 1
  producteur: string // 3
  url: string // 6
  url_data: string // 7
  version: string // 4
  id_reference: string | null
}

export interface Sources {
  [idReference: string]: Source
}

export interface SourcesData {
  education: Sources
  elus: Sources
  emplois: Sources
  entreprises: Sources
  equipements: Sources
  geographie: Sources
  logements: Sources
  meteo: Sources
  population: Sources
  sante: Sources
  environnement: Sources
  revenus: Sources
}
