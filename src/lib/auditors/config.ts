import {
  auditHttpUrl,
  auditRequire,
  cleanAudit,
  type Audit,
  auditSwitch,
  auditTrimString,
  auditStringToBoolean,
  auditBoolean,
  auditSetNullish,
  auditStringToNumber,
  auditInteger,
  auditFunction,
} from "@auditors/core"

export function auditConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["baseUrl", "dataUrl"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditHttpUrl,
      auditRequire,
    )
  }

  audit.attribute(data, "matomo", true, errors, remainingKeys, auditMatomo)

  for (const key of ["territoiresUrl"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditHttpUrl,
      auditRequire,
    )
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditMatomo(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "prependDomain",
    true,
    errors,
    remainingKeys,
    auditSwitch([auditTrimString, auditStringToBoolean], auditBoolean),
    auditSetNullish(false),
  )
  audit.attribute(
    data,
    "siteId",
    true,
    errors,
    remainingKeys,
    auditStringToNumber,
    auditInteger,
    auditRequire,
  )
  audit.attribute(
    data,
    "subdomains",
    true,
    errors,
    remainingKeys,
    auditTrimString,
  )
  audit.attribute(
    data,
    "url",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
    // Ensure there is a single trailing "/" in URL.
    auditFunction((url) => url.replace(/\/$/, "") + "/"),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function validateConfig(data: unknown): [unknown, unknown] {
  return auditConfig(cleanAudit, data)
}
