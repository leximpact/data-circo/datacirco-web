import "dotenv/config"
import * as process from "process"

import { validateConfig } from "$lib/auditors/config"

export interface Config {
  baseUrl: string
  dataUrl: string
  matomo?: {
    prependDomain?: boolean
    siteId: number
    subdomains?: string
    url: string
  }
  territoiresUrl: string
}

const [validConfig, error] = validateConfig({
  baseUrl: process.env["BASE_URL"],
  dataUrl: process.env["DATA_URL"],
  matomo:
    process.env["MATOMO_SITE_ID"] && process.env["MATOMO_URL"]
      ? {
          prependDomain: process.env["MATOMO_PREPEND_DOMAIN"],
          siteId: process.env["MATOMO_SITE_ID"],
          subdomains: process.env["MATOMO_SUBDOMAINS"],
          url: process.env["MATOMO_URL"],
        }
      : null,
  territoiresUrl: process.env["TERRITOIRES_URL"],
})
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(
      validConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

const config = validConfig as Config

export default config
