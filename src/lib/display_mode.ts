import {
  type Audit,
  auditSetNullish,
  auditStringToBoolean,
  auditTrimString,
  cleanAudit,
} from "@auditors/core"

import { auditQuerySingleton } from "$lib/auditors/queries"
import type { CircoModule } from "$lib/circo"

export interface DisplayMode {
  module?: CircoModule
  print?: boolean
}

function auditSimulationQuery(
  audit: Audit,
  query: URLSearchParams,
): [unknown, unknown] {
  if (query == null) {
    return [query, null]
  }
  if (!(query instanceof URLSearchParams)) {
    return audit.unexpectedType(query, "URLSearchParams")
  }

  const data: { [key: string]: unknown } = {}
  for (const [key, value] of query.entries()) {
    let values = data[key] as string[] | undefined
    if (values === undefined) {
      values = data[key] = []
    }
    values.push(value)
  }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "module",
    true,
    errors,
    remainingKeys,
    auditQuerySingleton(auditTrimString),
  )

  for (const key of ["mtm_campaign", "mtm_medium", "mtm_source"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditQuerySingleton(auditTrimString),
    )
  }

  audit.attribute(
    data,
    "print",
    true,
    errors,
    remainingKeys,
    auditQuerySingleton(auditTrimString, auditStringToBoolean),
  )

  return audit.reduceRemaining(data, errors, remainingKeys, auditSetNullish({}))
}

export function ensureValidDisplayMode(url: URL): DisplayMode {
  const [validQueryDisplayMode, queryError] = auditSimulationQuery(
    cleanAudit,
    url.searchParams,
  ) as [DisplayMode, unknown]

  if (queryError !== null) {
    console.warn(
      `Query error at ${url.pathname}: ${JSON.stringify(
        queryError,
        null,
        2,
      )}\n\n${JSON.stringify(validQueryDisplayMode, null, 2)}`,
    )
    return {}
  }

  return validQueryDisplayMode
}

export function queryFromDisplayMode(displayMode: DisplayMode): string {
  const parametersQuery: URLSearchParams = new URLSearchParams()
  if (displayMode.module) {
    parametersQuery.append("module", displayMode.module)
  }
  if (displayMode.print) {
    parametersQuery.append("print", "true")
  }
  const parametersQueryString = parametersQuery.toString()

  return `?${parametersQueryString}`
}
