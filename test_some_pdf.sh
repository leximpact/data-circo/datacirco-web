echo "Starting script at `date`"
set -e

mkdir -p pdf

read_env() {
  echo "Reading .env config"
  local filePath="${1:-.env}"

  if [ ! -f "$filePath" ]; then
    echo "missing ${filePath}"
    exit 1
  fi

  while read -r LINE; do
    # Remove leading and trailing whitespaces, and carriage return
    CLEANED_LINE=$(echo "$LINE" | awk '{$1=$1};1' | tr -d '\r')

    if [[ $CLEANED_LINE != '#'* ]] && [[ $CLEANED_LINE == *'='* ]]; then
      export "$CLEANED_LINE"
    fi
  done < "$filePath"
}

read_env

BASE_URL=`sed -e 's/^"//' -e 's/"$//' <<<"$BASE_URL"`

mkdir -p pdf
mkdir -p reduced-pdf
cd pdf

# for c in "976-01" "075-01" "091-07"
for c in $(cat ../circo_test.txt)
do
    echo "Enregistrement de $c en PDF..."
    # datacirco-web-integ.leximpact.dev
    echo "wget -O $c.pdf $BASE_URL/circo-print/$c"
    wget -O $c.pdf "$BASE_URL/circo-print/$c"
    echo "Reduce PDF size"
    gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile=../reduced-pdf/$c.pdf $c.pdf
done
echo "Done script `date` in $SECONDS s at `date`"