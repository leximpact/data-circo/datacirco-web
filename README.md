# DataCirco : interface Web et export PDF

Prototype de version Web disponible sur https://datacirco-web-integ.leximpact.dev/circonscriptions/091-07

## Installation

L'utilisation de NVM permet de gérer les versions de Node et de s'affranchir des limitations de version des distributions.

A exécuter dans le dossier du projet `cd datacirco-web` :

```sh
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
source ~/.bashrc
nvm install node
npm install
npx playwright install-deps
npx playwright install
cp example.env .env
```

## Developing

Start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

Go to http://127.0.0.1:5173/

### Using code-server

Run `npm run dev -- --host 0.0.0.0` in the VSCode terminal

Go to https://5173.code-server.leximpact.dev/

### Linting

Nous utilisont ESLint pour le linting, avec un plugin de tri des imports par ordre alphabétique.

For VS Code there is an option to execute `eslint --fix` when saving a file. For this to work you add this to your `settings.json` :

```json
{
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": "explicit"
  }
}
```

### View data

Les données brutes peuvent être visualisées à l'adresse suivante :
https://datacirco-web-integ.leximpact.dev/datacirco/data-json/091-07/common_data.json

Les sources sont les mémes pour toutes les circonscriptions : https://datacirco-web-integ.leximpact.dev/datacirco/data-json/sources.json

## Génération d'un PDF

Pour voir la page qui sera imprimée en PDF il faut ajouter le paramètre `print` dans l'URL : http://localhost:5173/circonscriptions/091-07?print=true

`wget -O 091-07.pdf http://localhost:5173/circo-print/091-07`

`export CIRCO=091-07; wget -O $CIRCO.pdf "http://localhost:5173/circo-print/$CIRCO"`

L'erreur _"waiting for locator('#precipitations').or(locator('#dpe')).first() to be visible"_ signifie que la circo n'existe probablement pas.

## Génération de tous les PDF

Il faut avoir installé :

- ImageMagick pour créer la vignette
- Ghostscript pour la reduction de taille du PDF
- optipng pour la réduction de taille du PNG
- qrcode pour la génération du QR code.

```sh
apt update && apt upgrade -y
apt -y install wget nano git curl optipng imagemagick-6.q16hdri ghostscript
pip install qrcode[pil]
```

```sh
generate_pdf.sh
```

Il faut compter 1h30.

Pour le détail de la production LexImpact, voir [La doc DevOps](https://git.leximpact.dev/leximpact/communs/devops/-/blob/master/notes_installations/datacirco.md) (accès restreint).

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
