/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{html,js,svelte,ts}"],
  plugins: [require("@tailwindcss/forms")],
  theme: {
    extend: {
      animation: {
        "pulse-2": "pulse-2 1.7s cubic-bezier(0.4, 0, 0.6, 1) infinite",
      },
      colors: {
        datacirco: "#7f7a09",
        "le-bleu": "#343bff",
        "le-bleu-light": "#d2dfff",
        "le-jaune-light": "#EEEA8A",
        "le-jaune": "#ded500",
        "le-jaune-dark": "#9d970b",
        "le-jaune-very-dark": "#6E6A08",
        "le-rouge-bill": "#ff6b6b",
        "le-gris-dispositif": "#5E709E",
        "le-gris-dispositif-ultralight": "#EBEFFA",
        "le-gris-dispositif-light": "#CCD3E7",
        "le-gris-dispositif-dark": "#2F406A",
        "le-vert-validation": "#13CC03",
        "le-vert-validation-dark": "#377330",
        "le-vert": {
          50: "#f1f0e6",
          100: "#e2e1cd",
          200: "#c5c39c",
          300: "#a8a66a",
          400: "#8b8839",
          500: "#6e6a07",
          600: "#635f06",
          700: "#585506",
          800: "#424004",
          900: "#2c2a03",
          950: "#161501",
        },
      },
    },
    fontFamily: {
      sans: ["Lato", "sans-serif"],
      serif: ["Lora", "serif"],
    },
    keyframes: {
      "pulse-2": {
        "0%, 100%": {
          opacity: 1,
        },
        "50%": {
          opacity: 0.2,
        },
      },
    },
  },
}
