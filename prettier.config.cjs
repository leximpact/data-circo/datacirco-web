module.exports = {
  overrides: [{ files: "*.svelte", options: { parser: "svelte" } }],
  plugins: ["prettier-plugin-svelte"],
  semi: false,
  trailingComma: "all",
}
