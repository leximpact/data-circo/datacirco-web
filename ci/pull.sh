#!/bin/bash
# Stop at first error
set -e

CI_COMMIT_REF_NAME=$1
CI_COMMIT_SHA=$2
if [ -z "$CI_COMMIT_REF_NAME" ]
then
      echo "CI_COMMIT_REF_NAME is empty !"
      exit 1
fi
if [ -z "$CI_COMMIT_SHA" ]
then
      echo "CI_COMMIT_SHA is empty !"
      exit 1
fi
echo "Starting deploy for branch $CI_COMMIT_REF_NAME at commit $CI_COMMIT_SHA"
echo "Fetching from Gitlab..."
git fetch --all
echo "Reset git env..."
git reset --hard origin/$CI_COMMIT_REF_NAME
echo "Checkout..."
git checkout -B $CI_COMMIT_REF_NAME $CI_COMMIT_SHA
echo "Pull..."
git pull origin $CI_COMMIT_REF_NAME
